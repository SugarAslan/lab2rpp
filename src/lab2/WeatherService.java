package lab2;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;


class WeatherService {

    public static Queue<Task> inq = new LinkedList<>();


    static class TaskExecutor implements Runnable{

        static Random random = new Random();
        static Queue<Task> outq = new LinkedList<>();

        public void  execute () {

            Task element;


            while(!inq.isEmpty()){



                synchronized (inq) {
                    if(inq.isEmpty()) break;
                    element = inq.remove();
                }

                int temperature = random.nextInt(50) - 25;
                element.setWeather((short)temperature);

                synchronized (outq){
                    outq.add(element);
                }


//                System.out.println(Thread.currentThread().getName());

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    return;
                }

            }
            System.out.println("End");

        }

        public static Task taskReq(){
            Task element = outq.remove();
            return element;
        }

        @Override
        public void run() {

            execute();

        }
    }

}



class Task {
    private int id;
    private String city = "Moscow";
    private LocalDate date;
    private short weather;



    Task(int id, LocalDate date) {

        this.id = id;
        this.date = date;
    }

    @Override
    public String toString() {
        return getId() + " " + getCity() + " " + getDate() + " " + getWeather();
    }

    public void setWeather(short weather) {
        this.weather = weather;
    }


    public int getId() {
        return id;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getCity() {
        return city;
    }

    public short getWeather() {
        return weather;
    }
}