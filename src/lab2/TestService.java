package lab2;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;



public class TestService {
    public static void main(String[] args) {

        int taskCounter = 400;

        Task testTask = taskTestGen(taskCounter);
        Task element;

        taskGen(taskCounter);

        WeatherService.TaskExecutor te = new WeatherService.TaskExecutor();

        Thread t1 = new Thread(te);
        Thread t2 = new Thread(te);
        Thread t3 = new Thread(te);
        Thread t4 = new Thread(te);
        Thread t5 = new Thread(te);
        Thread t6 = new Thread(te);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();




        while (t1.isAlive() || t2.isAlive() || t3.isAlive() || t4.isAlive() || t5.isAlive() || t6.isAlive()){

        }



        if (WeatherService.TaskExecutor.outq.size() == taskCounter) System.out.println("Number of elements: " + taskCounter);
        else System.out.println("Error of task number");

        element = WeatherService.TaskExecutor.outq.remove();
        if( element.equals(testTask) ) System.out.println("Weather in " + testTask.getDate() + " was " + testTask.getWeather());
        else System.out.println("Loser");
        System.out.println(element);

        while (!WeatherService.TaskExecutor.outq.isEmpty()){

            Task ans = WeatherService.TaskExecutor.taskReq();
            System.out.println(ans);

        }

        if(WeatherService.TaskExecutor.outq.isEmpty()) System.out.println("Out of Tasks \n");
        else System.out.println("Error of threads");
    }



    private static void taskGen(int taskCount) {

        Random random = new Random();

        for(int i = 2; i < taskCount + 1; i++) {

            int year = random.nextInt(10)+ 2008;
            int month = random.nextInt(11)+ 1;
            int day = random.nextInt(27)+ 1;

            Task task = new Task(i, LocalDate.of(year, month, day));
            WeatherService.inq.add(task);
        }
    }

    private static Task taskTestGen(int taskCount) {

        int year = 1999;
        int month = 8;
        int day = 4;

        Task task = new Task(1, LocalDate.of(year, month, day));
        task.setWeather((short)27);

        WeatherService.TaskExecutor.outq.add(task);
        return task;
    }
}


